//Name:Brandon Mathis
//File Name:HillCipher.cpp
//Date:March 8, 2016
//Program Description:Encryption/Decryption using Hill Cipher

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <iomanip>

using namespace std;
int getPlainText(ifstream& input, string& plainText);
int getCipherText(ifstream& input, string& cipherText);
void encrypt(string plainText,int& length,int encryption[],string& cipherText);
void getInverse(int encryption[], int decryption[]);
void decrypt(string& plainText,int length,int encryption[],string cipherText);
void writeMatrix(int matrix[],ostream& output);
void writeText(string text,ostream& output, int textLength);

int main ()
	{
	char choice;
	int encryption[4];
	int decryption[4];
	int length;
	ifstream input;
	ofstream outFile;
	string plainText;
	string cipherText;
	string inFileName;
	string outFileName;

	cout << "Encrypt or Decrypt(E/D)? ";
	cin >> choice;
	cin.ignore(999,'\n');
	if(islower(choice))
		choice = choice + 'A' - 'a';

	cout << "Enter name for source file: ";
	getline(cin,inFileName);
	input.open(inFileName.c_str());
	if(input.fail())
		{
		cout << "File not found" << endl;
		exit(1);
		}
	cout << "Enter name for destination file: ";
	getline(cin,outFileName);
	cout << "Enter values for encryption matrix by row: ";
	for(int n = 0; n < 4; n++)
		cin >> encryption[n];

	if(choice == 'E')
		{
		length = getPlainText(input, plainText);
		input.close();
		encrypt(plainText,length,encryption,cipherText);
		outFile.open(outFileName.c_str());
		writeText(cipherText,cout,length);
		writeText(cipherText,outFile,length);
		outFile.close();
		}
	else if(choice == 'D')
		{
		length = getCipherText(input, cipherText);
		input.close();
		getInverse(encryption,decryption);
		decrypt(plainText,length,decryption,cipherText);
		outFile.open(outFileName.c_str());
		writeMatrix(decryption,cout);
		writeMatrix(decryption,outFile);
		writeText(plainText,cout,length);
		writeText(cipherText,outFile,length);
		outFile.close();
		}
	else
		input.close();

	}
//=====================================================
//Reads input file into plainText string
int getPlainText(ifstream& input, string& plainText)
	{
	int length;
	char temp;
	plainText = "";
	length = 0;
	while(input.get(temp))
		{
		if(isupper(temp))					//Convert all text to lowercase
			temp = temp - 'A' + 'a';
		if(temp >= 'a' && temp <= 'z')
			{
			plainText = plainText + temp;
			length++;
			}
		}
	return length;
	}
//=====================================================
//Reads input file into cipherText string
int getCipherText(ifstream& input, string& cipherText)
	{
	int length;
	char temp;
	cipherText = "";
	length = 0;
	while(input.get(temp))
		{
		if(islower(temp))					//Convert all text to uppercase
			temp = temp + 'A' - 'a';
		if(temp >= 'A' && temp <= 'Z')
			{
			cipherText = cipherText + temp;
			length++;
			}
		}
	return length;
	}
//=====================================================
//Encrypts given plain text using encryption matrix
void encrypt(string plainText,int& length,int encryption[],string& cipherText)
	{
	int numGroups;
	int cipher;
	int oddNum;
	cipherText = "";
	if(length%2 == 1)						//If the text has an odd length append an 'x' to it
		{
		oddNum = 1;
		plainText = plainText + 'x';
		length++;
		}
	else
		oddNum = 0;
	numGroups = length/2 + oddNum;
	plainText = plainText + '\0';
	for(int n = 0; n < numGroups; n++)
		{
		cipher= encryption[0] * (plainText[n*2]-'a'+ 1) + encryption[1] * (plainText[n*2+1]-'a'+1);	//C1 = A * P1 + B * P2
		cipher = (cipher-1) %26 + 'A';
		cipherText = cipherText + (char)cipher;

		cipher= encryption[2] * (plainText[n*2]-'a'+1) + encryption[3] * (plainText[n*2+1]-'a'+1); 	//C2 = C * P1 + D * P2
		cipher = (cipher-1) %26 + 'A';
		cipherText = cipherText + (char)cipher;
		}
	cipherText = cipherText + '\0';
	}
//=====================================================
//Decrypts given cipher text using decryption matrix
void decrypt(string& plainText,int length,int decryption[],string cipherText)
	{
	int numGroups;
	int plain;
	int oddNum;
	plainText = "";
	numGroups = length/2;
	cipherText = cipherText + '\0';
	for(int n = 0; n < numGroups; n++)
		{
		plain= decryption[0] * (cipherText[n*2]-'A'+ 1) + decryption[1] * (cipherText[n*2+1]-'A'+1);	//P1 = A * C1 + B * C2
		plain = (plain-1) %26 + 'a';
		plainText = plainText + (char)plain;

		plain= decryption[2] * (cipherText[n*2]-'A'+1) + decryption[3] * (cipherText[n*2+1]-'A'+1);		//P2 = C * C1 + D * C2
		plain = (plain-1) %26 + 'a';
		plainText = plainText + (char)plain;
		}
	plainText = plainText + '\0';
	}
//=====================================================
//Gets the inverse of given encryption matrix
 void getInverse(int encryption[], int decryption[])
	{
	int determinant;
	int reciprocal;
	int temp;
	int possibleInverses[] = {1,3,5,7,9,11,15,17,19,21,23,25};
	determinant = encryption[0] * encryption[3] - encryption[1] * encryption[2];
	if(determinant % 2 == 0 || determinant % 13 == 0 )			//If determinant is not relatively prime to 26
		{
		cout << "There is no inverse for input encryption matrix" << endl;
		exit(1);
		}
	determinant = determinant % 26;
	for(int n = 0; n < 12; n++)
		{
		temp = determinant * possibleInverses[n];
		if(temp % 26 == 1)
			reciprocal = possibleInverses[n];
		}
	decryption[0] = (encryption[3] * reciprocal) % 26;			//Creates inverse matrix
	decryption[1] = (encryption[1] * (-1) * reciprocal) % 26;
	decryption[2] = (encryption[2] * (-1) * reciprocal) % 26;
	decryption[3] = (encryption[0] * reciprocal) % 26;
	if(decryption[1] < 0)
		decryption[1] = decryption[1] + 26;
	if(decryption[2] < 0)
		decryption[2] = decryption[2] + 26;
	}
//=====================================================
//Displays the text on screen or writes to file
void writeMatrix(int matrix[],ostream& output)
	{
	cout << "Decryption Matrix: " << endl;
	output << " --    --" << endl;
	output << " |" << setw(2) << matrix[0] << "  "<< setw(2) << matrix[1] << "|" << endl;
	output << " |" << setw(2) << matrix[2] << "  "<< setw(2) << matrix[3] << "|" << endl;
	output << " --    --" << endl;
	}
//=====================================================
//Displays the text on screen or writes to file
void writeText(string text,ostream& output, int textLength)
	{
	output << endl;
	for(int n = 0; n < textLength; n++)
		{
		output << text[n];
		if((n+1) % 5 == 0)
			output << " ";
		if((n+1)%20 == 0)
			output << endl;
		if((n+1) % 100 == 0)
			output << endl;
		}
	output << endl;
	}