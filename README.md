# Hill_Cipher

[Hill_Cipher](https://gitlab.com/bamathis/Hill_Cipher) is an implemention of the Hill cipher for encryption and decryption.

## Quick Start

### Program Execution

#### Encrypting/Decrypting
```
$ ./HillCipher.cpp 
```

### Known Issues

- The given input file must only consist of letters and spaces. 
